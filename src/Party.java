import java.util.ArrayList;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

@XmlRootElement
public class Party {

	@XmlAttribute
	public String date; 
	
	@XmlElement (name = "guest")
	public ArrayList<Guest> g; 
	
	public Party() {
		
	}

	@XmlTransient
	public ArrayList<Guest> getG() {
		return g;
	}

	public void setG(ArrayList<Guest> g) {
		this.g = g;
	}
}
