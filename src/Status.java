import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlTransient;


@XmlAccessorType(XmlAccessType.FIELD)
public class Status {

	@XmlAttribute
	private String single;
	@XmlAttribute
	private String sober;
	
	public Status() {
	
	}

	@XmlTransient
	public String getSingle() {
		return single;
	}

	public void setSingle(String single) {
		this.single = single;
	}

	@XmlTransient
	public String getSober() {
		return sober;
	}

	
	public void setSober(String sober) {
		this.sober = sober;
	}
}
