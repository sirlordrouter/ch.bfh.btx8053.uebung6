import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.dom.DOMSource;
import javax.xml.validation.SchemaFactory;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;


public class PartySerializer {

	/**
	 * @param args
	 * @throws JAXBException 
	 * @throws IOException 
	 * @throws SAXException 
	 * @throws ParserConfigurationException 
	 */
	public static void main(String[] args) throws JAXBException, IOException, SAXException, ParserConfigurationException {
		// TODO Auto-generated method stub

		Party p = new Party();
		p.date = "2012-09-30";
		ArrayList<Guest> gs = new ArrayList<Guest>();
		Guest g1 = new Guest();
		g1.setName("Albert");
		g1.setDrinks(new ArrayList<String>(Arrays.asList("wine", "beer")));
		Status s1 = new Status();
		s1.setSingle("true");
		s1.setSober("false");
		g1.setStatus(s1);
		gs.add(g1);
		
		Guest g2 = new Guest();
		g2.setDrinks(new ArrayList<String>(Arrays.asList("Apfelsaft")));
		g2.setName("Martina");
		Status s2 = new Status();
		s2.setSingle("true");
		s2.setSober("true");
		g2.setStatus(s2);
		gs.add(g2);
		
		
		p.setG(gs);
		
		
		FileWriter writer = new FileWriter("PartyOut.xml"); 
		JAXBContext context = JAXBContext.newInstance(Party.class); 
		Marshaller m = context.createMarshaller(); 
		m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE); 
		m.marshal(p, writer);
		
		Document dom = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse("PartyOut.xml");
		SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI)
				.newSchema(new File("src/Party.xsd")).newValidator().validate(new DOMSource(dom));

		
		Unmarshaller u = context.createUnmarshaller();
		Party partyOut = (Party) u.unmarshal(new File("PartyOut.xml"));
		
		for (Guest go : partyOut.getG()) {
			System.out.println(go.getName());
			
		}
		
	}

}
