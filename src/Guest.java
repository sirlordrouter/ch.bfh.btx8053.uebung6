import java.util.ArrayList;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlTransient;


@XmlAccessorType(XmlAccessType.FIELD)
public class Guest {
	
	@XmlAttribute
	private String name; 
	
	@XmlElement
	private Status status;
	
	@XmlElementWrapper (name = "drinks") 
	@XmlElement (name = "drink")
	private ArrayList<String> drinks;
	
	public Guest() {
		
	}
	
	@XmlTransient
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@XmlTransient
	public ArrayList<String> getDrinks() {
		return drinks;
	}

	public void setDrinks(ArrayList<String> drinks) {
		this.drinks = drinks;
	}

	@XmlTransient
	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}
	
}
